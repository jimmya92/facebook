import Vapor

struct WrappedResponse<T: Content>: Content {
    
    var data: T?
}
