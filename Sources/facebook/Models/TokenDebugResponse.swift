import Vapor

struct TokenDebugResponse: Content {
    
    var appId: String?
    var isValid: Bool
    
    enum CodingKeys: String, CodingKey {
        case appId = "app_id"
        case isValid = "is_valid"
    }
}
