import Vapor

public enum ImageSize: String, Content {
    
    case large
    case normal
    case small
    case square
}
