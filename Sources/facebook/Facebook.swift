import Vapor

// MARK: - Service

public protocol FacebookProvider: Service {
    var appID: String { get }
    var appSecret: String { get }
    func verify(_ accessToken: String, on req: Request) throws -> Future<Void>
    func getUserInfo<T: Decodable>(_ accessToken: String, fields: [String], responseType: T.Type, on req: Request) throws -> Future<T>
    func getUserImage(_ userID: String, size: ImageSize, on req: Request) throws -> Future<Data>
}

// MARK: - Engine

public struct Facebook: FacebookProvider {
    
    public let appID: String
    public let appSecret: String
    
    // MARK: Initialization
    
    public init(appID: String, appSecret: String) {
        self.appID = appID
        self.appSecret = appSecret
    }
    
    // MARK: Send message
    
    public func verify(_ accessToken: String, on req: Request) throws -> Future<Void> {
        let appID = self.appID
        let client = try req.make(Client.self)
        let url = "https://graph.facebook.com/debug_token?input_token=\(accessToken)&access_token=\(appID)%7C\(appSecret)"
        return client.get(url).flatMap(to: WrappedResponse<TokenDebugResponse>.self) { response in
            guard response.http.status == .ok else {
                throw Abort(.internalServerError)
            }
            return try response.content.decode(WrappedResponse<TokenDebugResponse>.self)
            }.map(to: Void.self) { tokenDebug in
                guard tokenDebug.data?.appId == appID && tokenDebug.data?.isValid == true else {
                    throw Abort(.badRequest)
                }
                return
        }
    }
    
    public func getUserInfo<T>(_ accessToken: String, fields: [String], responseType: T.Type, on req: Request) throws -> Future<T> where T: Decodable {
        let fieldUrlComponent = fields.joined(separator: ",")
        let client = try req.make(Client.self)
        let meUrl = "https://graph.facebook.com/me?access_token=\(accessToken)&fields=\(fieldUrlComponent)"
        return client.get(meUrl).flatMap(to: responseType) { response in
            guard response.http.status == .ok else {
                throw Abort(.internalServerError)
            }
            return try response.content.decode(responseType)
        }
    }
    
    public func getUserImage(_ userID: String, size: ImageSize, on req: Request) throws -> Future<Data> {
        let client = try req.make(Client.self)
        let imageUrl = "https://graph.facebook.com/\(userID)/picture?type=\(size.rawValue)"
        return client.get(imageUrl).map(to: Data.self) { response in
            guard response.http.status == .ok else {
                throw Abort(.internalServerError)
            }
            guard let data = response.http.body.data else {
                throw Abort(.internalServerError)
            }
            return data
        }
    }
}
