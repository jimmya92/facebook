import XCTest

import facebookTests

var tests = [XCTestCaseEntry]()
tests += facebookTests.allTests()
XCTMain(tests)
